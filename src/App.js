import React from 'react';
import './App.css';
import PresentationalComponent from './presentational';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      checked: false,
      txtValue: '',
    }
  }

  renderPresentation = () => {
    this.setState({
      checked: !this.state.checked,
    });
  }

  updateTextInput = (e) => {
    this.setState({
      txtValue: e.target.value,
    });
  }

  render() {
    return (
      <div className="App">
          <PresentationalComponent
            renderText={this.state.txtValue}
            updateTextInput={(e)=>this.updateTextInput(e)}
            renderPresentation={this.renderPresentation()}
            checked={this.state.checked}
          />
      </div>
    );
  }
};
