import React from 'react';

export default function PresentationalComponent({
    renderText,
    updateTextInput,
    renderPresentation,
    checked,
}) {
    return (
        <div>
        <input
          type='text'
          onChange={(e) => updateTextInput(e)}
          placeholder='enter some text'
        />
        <div>
          <input
            type='checkbox'
            value={checked}
          />
        </div>
        <input
          type='submit'
          value='submit'
          onClick={() => renderPresentation()}
        />
        {renderText}
      </div>
    )
}